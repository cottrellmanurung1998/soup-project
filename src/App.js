import { BrowserRouter, Routes, Route } from "react-router-dom";
import Home from "./pages/Home";
import About from "./pages/About";
import Contact from "./pages/Contact";
import Menu from "./pages/Menu";
import Pagenotfound from "./pages/Pagenotfound";
import Myclass from "./pages/Myclass";
import Checkout from "./pages/Checkout";
import Invoice from "./pages/Invoice";
import Profile from "./pages/Profile";
import Detailinvoice from "./pages/Detailinvoice";
import SuccessPayment from "./pages/Successpayment";
import Landingpage from "./pages/landingpage";
import Login from "./pages/login";
import Register from "./pages/Register";
import CourseAsian from "./pages/courseasian";
import Resetpw from "./pages/resetpw";
import Reset from "./pages/resetpw";
import Tomyum from "./pages/tomyum";
import Createpw from "./pages/createpw";

function App() {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Landingpage />} />
          {/* <Route path="/login" element={<Login />} />
          <Route path="/register" element={<Register />} /> */}
          <Route path="/home" element={<Home />} />
          <Route path="/about" element={<About />} />
          <Route path="/contact" element={<Contact />} />
          <Route path="/menu" element={<Menu />} />
          <Route path="*" element={<Pagenotfound />} />
          <Route path="/myclass" element={<Myclass />} />
          <Route path="/checkout" element={<Checkout />} />
          <Route path="/invoice" element={<Invoice />} />
          <Route path="/profile" element={<Profile />} />
          <Route path="/invoice/detailinvoice" element={<Detailinvoice />} />
          <Route path="/checkout/success" element={<SuccessPayment />} />
          <Route path="/login" element={<Login />} />
          <Route path="/register" element={<Register />} />
          <Route path="/courseasian" element={<CourseAsian />} />
          <Route path="/resetpw" element={<Resetpw />} />
          <Route path="/resetpw" element={<Reset />} />
          <Route path="/" element={<Landingpage />} />
          <Route path="/tomyum" element={<Tomyum />} />
          <Route path="/createpw" element={<Createpw />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
