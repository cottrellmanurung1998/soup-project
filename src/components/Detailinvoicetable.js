import React from "react";
import {
  Box,
  Container,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
  Paper,
  styled, // Import the styled function
} from "@mui/material";
import { useParams } from "react-router-dom";

// Create a custom styled component for the header cells
const GoldHeaderTableCell = styled(TableCell)(({ theme }) => ({
  backgroundColor: "#5B4947", // Gold background color
  color: theme.palette.common.white, // White text color
  fontWeight: 600,
}));

const DetailInvoice = () => {
  const { id } = useParams();

  const headerColumns = [
    {
      title: "No",
      props: {},
    },
    {
      title: "Course Name",
      props: {
        align: "center",
      },
    },
    {
      title: "Type",
      props: {
        align: "center",
      },
    },
    {
      title: "Schedule",
      props: {
        align: "center",
      },
    },
    {
      title: "Price",
      props: {
        align: "center",
      },
    },
  ];

  const rowData = [
    {
      no: 1,
      course_name: "Hyundai Palisade 2021",
      type: "SUV",
      schedule: "Wednesday, 27 July 2022",
      price: "IDR 850.000",
    },
  ];

  return (
    <Container maxWidth="xl" sx={{ marginBottom: "207px" }}>
      <Box sx={{ marginBottom: "32px" }}>
        <Typography variant="h5" color="text.secondary" sx={{ fontWeight: 600, marginBottom: "24px" }}>
          Details Invoice
        </Typography>
        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "end",
            marginBottom: "42px",
          }}
        >
          <Box width={"250px"}>
            <Table>
              <TableBody>
                <TableRow>
                  <TableCell sx={{ padding: 0, fontSize: "16px", border: 0 }} align="left">
                    No Invoice :
                  </TableCell>
                  <TableCell sx={{ padding: 0, fontSize: "16px", border: 0 }} align="left">
                  SOU00003
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell sx={{ padding: 0, fontSize: "16px", border: 0 }} align="left">
                    Date :
                  </TableCell>
                  <TableCell sx={{ padding: 0, fontSize: "16px", border: 0 }} align="left">
                    12 June 2022
                  </TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </Box>
          <Box>
            <Typography
              component="p"
              sx={{ fontSize: "18px", fontWeight: 600, marginRight: "24px" }}
            >
              Total Price <Typography variant="span">IDR 850.000</Typography>
            </Typography>
          </Box>
        </Box>
        <TableContainer component={Paper}>
          <Table sx={{ minWidth: 700 }} aria-label="customized table">
            <TableHead>
              <TableRow>
                {headerColumns.map((col, index) => (
                  <GoldHeaderTableCell key={index} {...col.props}>
                    {col.title}
                  </GoldHeaderTableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {rowData.map((row, index) => (
                <TableRow key={index}>
                  <TableCell component="th" scope="row">
                    {row.no}
                  </TableCell>
                  <TableCell align="center">{row.course_name}</TableCell>
                  <TableCell align="center">{row.type}</TableCell>
                  <TableCell align="center">{row.schedule}</TableCell>
                  <TableCell align="center">{row.price}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Box>
    </Container>
  );
};

export default DetailInvoice;
