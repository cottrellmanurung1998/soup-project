import React from 'react'
import Headertwo from './Headertwo'

const Layouttwo = ({ children }) => {
  return (
    <>
        <Headertwo />
        <div>
            {children}
        </div>
    </>
  )
}

export default Layouttwo