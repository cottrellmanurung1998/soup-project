import React, { } from "react";
import {
  List,
  ListItemButton,
  ListItemContent,
  ListItemDecorator,
} from "@mui/joy";
import { Box } from "@mui/material";

export default function ListPayment({ items, setIndex,index,setSelectedItem  }) {

  return (
    <Box sx={{ width: "100%", maxWidth: 360, bgcolor: "background.paper" }}>
      <List
        component="nav"
        aria-label="main mailbox folders"
        sx={{
          "--ListItemDecorator-size": "70px",
          "--ListItemDecorator-marginY": "10px",
        }}
      >
        {items.map((item)=>(
                  <ListItemButton
                  selected={index === item.id}
                  variant={index === item.id ? "soft" : "plain"}
                  sx={ index === item.id ? {backgroundColor:'#bdbdbd'} : undefined }
                  onClick={()=> {
                    setSelectedItem(item.id)
                    setIndex(item.id)
                  }}
                >
                  <ListItemDecorator>
                    <img src={`/${item.picture}`} alt={item.pay} />
                  </ListItemDecorator>
                  <ListItemContent>{item.pay}</ListItemContent>
                </ListItemButton>
        ))}
      </List>
    </Box>
  );
}
