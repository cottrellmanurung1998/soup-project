import React, { useState } from "react";
import { Box, Modal, Typography, Button } from "@mui/material";
import Hooks from '../../Hooks/hooks';
import ListPayment from "./listPayment";

const modalStyle = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 350,
  bgcolor: "background.paper",
  boxShadow: 24,
  p: 4,
};

export default function ModalPayment({
  open,
  handleClose,
  handlePayment,
  itemsList,
}) {
  

    // data output payment_method jika selected dan button clicked


    const [select, setselect] = useState(0);
    const url = "http://localhost:5138/PaymentMethod"
    const paymentList = Hooks (url);
    //handle untuk output payment
       // data output payment_method jika selected dan button clicked
       const [selectedPaymentMethod, setSelectedPaymentMethod] = useState(paymentList[0]);
       const HandlePaymentButton = () => {
        handlePayment(selectedPaymentMethod, itemsList) 
       } 

  return (
    <Modal
      open={open}
      onClose={handleClose}
      aria-labelledby="modal-modal- "
      aria-describedby="modal-modal-description"
    >
      <Box sx={modalStyle}>
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            marginBottom: "5px",
          }}
        >
          <Typography
            id="modal-modal-title"
            variant="h6"
            component="h2"
            sx={{ fontSize: "20px", lineHeight: "30px" }}
          >
            Select Payment Method
          </Typography>
        </Box>
        <Box sx={{ width: "100%" }}>
          <ListPayment items={paymentList} setIndex={setselect} index={select} setSelectedItem={setSelectedPaymentMethod}/>
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-around",
              marginTop: "10px",
            }}
          >
            <Button
              variant="contained"
              sx={{
                marginRight: "10px",
                backgroundColor: "#EA9E1F",
                minWidth: "130px",
              }}
              onClick={handleClose}
            >
              Cancel
            </Button>
            <Button
              variant="contained"
              sx={{
                backgroundColor: "#226957",
                fontFamily: "Montserrat",
                minWidth: "130px",
              }
            }
            onClick={HandlePaymentButton}
            >
              Pay Now
            </Button>
          </Box>
        </Box>
      </Box>
    </Modal>
  );
}
