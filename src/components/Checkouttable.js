import React, { useState, useEffect } from "react";
import { Box, Grid, Typography, Checkbox } from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import OvoLogo from "../images/ovo.jpg";
import CheckoutFooter from "../components/CheckoutFooter";
import ModalPayment from "../components/modalPayment";
import Hooks from '../Hooks/hooks'
import axios from "axios";
import { useNavigate } from "react-router-dom";

export default function Checkout() {
  const [SelectAll, setSelectAll] = useState(false);
  const [checkedItems, setCheckedItems] = useState([]);
  const [isPaymentOpen, setIsPaymentOpen] = useState(false);
  const [TotalPrice, setTotalPrice] = useState(0);
  const history = useNavigate();


  const url = "http://localhost:5138/Checkout"
  const CheckoutList = Hooks (url);

  useEffect(() => {
    if (SelectAll) {
      setCheckedItems([...CheckoutList]);
      calculateTotalPrice(CheckoutList);
    } else {
      setCheckedItems([]);
      calculateTotalPrice([]);
    }
  }, [SelectAll, CheckoutList]);

  const handleCheckAllChange = (event) => {
    setSelectAll(event.target.checked);
  };

  const handleCheckedItem = (item, isChecked) => {
    if (isChecked) {
      setCheckedItems([...checkedItems, item]);
      calculateTotalPrice([...checkedItems, item]);
    } else {
      setCheckedItems(checkedItems.filter((checkedItem) => checkedItem.id !== item.id));
      calculateTotalPrice(checkedItems.filter((checkedItem) => checkedItem.id !== item.id));
    }
  };

  const calculateTotalPrice = (items) => {
    let total = 0;
    for (let i = 0; i < items.length; i++) {
      total += items[i].price;
    }
    setTotalPrice(total);
  };

  const handlePaymentOpen = () => {
    setIsPaymentOpen(true);
  };

  const handlePaymentClose = () => {
    setIsPaymentOpen(false);
  };
  
  const submit = async (selectpaymentmethod, itemsList) => {
    const arrayUser_Course = itemsList.map(
      (item) => (
          {
            id : 0,
            picture: item.picture,
            type: item.type,
            courseName: item.course,
            schedule: item.schedule,
          }
      )   
  );
  const invoice = itemsList.map((item) => {
          
    return {
      Order_Date: new Date(),
      Total_Course: itemsList.length,
      Total_Price: itemsList.reduce((total, item) => total + item.price, 0),
      Id_Payment: selectpaymentmethod,
      type: item.type,
      courseName: item.course,
      schedule: item.schedule
  }
  })


    const posturl = "http://localhost:5138/MyClass/Post";
    const postInvoiceUrl = "http://localhost:5138/Invoices";
    
      await axios.post(posturl, arrayUser_Course)
      .then((Response)=> {
        return Response.data
      })
      .catch((error)=> {
        console.log (error)
      })

      await axios.post(postInvoiceUrl, invoice)
      .then((Response)=> {
        return Response.data
      })
      .catch((error)=> {
        console.log (error)
      })

      setTimeout(() => {
        history("/checkout/success");
    }, 2500);
  }

  return (
    <>
      <Grid
        container
        spacing={0}
        direction="column"
        alignItems="center"
        sx={{
          width: "100%",
          minHeight: "500px",
        }}
      >
        <Box
          sx={{
            display: 'flex',
            borderBottom: 1,
            borderColor: "black",
            width: "80%",
            minHeight: "10px",
            marginTop: "10px",
            marginX: "50px",
            paddingBottom: "10px",
          }}
        >
          <Checkbox
            checked={SelectAll}
            onChange={handleCheckAllChange}
            disableRipple
            color="primary"
            sx={{
              marginRight: "10px",
            }}
          />
          <Typography
            sx={{
              fontWeight: 300,
              fontSize: '20px',
            }}
          >
            Pilih Semua
          </Typography>
        </Box>
        {CheckoutList.length > 0 ? (
          CheckoutList.map((product, index) => {
          const isChecked = checkedItems.includes(product);
          return (
            <Box
              key={index}
              justifyContent="space-between"
              alignItems='center'
              sx={{
                display: 'flex',
                borderBottom: 1,
                borderColor: "black",
                width: "80%",
                minHeight: "20px",
                marginTop: "20px",
                marginX: "50px",
                paddingBottom: "20px",
                paddingRight: "10px",
              }}
            >
              <Checkbox
                checked={isChecked}
                onChange={(event) => handleCheckedItem(product, event.target.checked)}
                disableRipple
                color="primary"
                sx={{
                  marginRight: "10px",
                }}
              />
              <Grid
                container
                spacing={0}
                sx={{
                  display: 'flex',
                }}
              >
                <img
                  src={product.picture}
                  alt={product.type}
                  width="200px"
                  height="135px"
                  sx={{
                    // border: 2,
                    // borderColor:"blue",
                  }}
                />
                <Box
                  justifyItems='space-evenly'
                  sx={{
                    // border: 1,
                    // borderColor:"yellow",
                    marginLeft: '20px',
                  }}
                >
                  <Typography
                    gutterBottom
                    variant="span"
                    color="text.secondary"
                    component="div"
                    marginBottom="10px"
                  >
                    {product.course}
                  </Typography>
                  <Typography
                    variant="h6"
                    color="text.primary"
                    marginBottom="10px"
                    sx={{ fontWeight: 600 }}
                  >
                    {product.type}
                  </Typography>
                  <Typography
                    gutterBottom
                    variant="span"
                    color="text.secondary"
                    component="div"
                    marginBottom="10px"
                  >
                    {product.schedule}
                  </Typography>
                  <Typography
                    variant="h6"
                    color="primary"
                    sx={{ fontWeight: 600 }}
                  >
                    IDR {product.price}
                  </Typography>
                </Box>
              </Grid>
            </Box>
          );
        })):(<div>No data available</div>)}
      </Grid>
      <CheckoutFooter
        priceCount={TotalPrice}
        onclick={handlePaymentOpen}
      />
      <ModalPayment open={isPaymentOpen} handleClose={handlePaymentClose}
      itemsList={checkedItems}
      handlePayment={submit} />
    </>
  );
}
