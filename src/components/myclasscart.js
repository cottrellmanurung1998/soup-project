import React from "react";
import {
  Box,
  Typography,

  Card,
  CardContent,
  CardMedia,
  useTheme,
  useMediaQuery,
} from "@mui/material";
export default function ClassList({ item }) {
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("sm"));

  return (
    <Card
      sx={{
        display: "flex",
        flexDirection: isMobile ? "column" : "row",
        width: "100%",
        height: isMobile ? 'auto' : '170px',
        marginBottom: "20px",
      }}
    >

      <CardMedia
        component="img"
        sx={{
          width: isMobile ? "100%" : "250px",
          height: isMobile ? "auto" : "auto",
        }}
        image={`/${item.picture}`}
        alt="Class Cover"
      />
      <Box sx={{ display: "flex", flexDirection: "column", width: "100%",marginTop: isMobile ? 'auto' : '15px' }}>
        <CardContent sx={{ flex: "1 0 auto" }}>
          <Typography
            variant="overline"
            color="text.secondary"
            component="div"
            sx={{ textAlign: "left",fontFamily: "Montserrat" }}
          >
            {item.type}
          </Typography>
          <Typography variant="h5" component="div" sx={{ textAlign: "left",fontFamily: "Montserrat",fontWeight:900 }}>
            {item.courseName}
          </Typography>
          <Typography
            variant="subtitle2"
            color="#EA9E1F"
            component="div"
            sx={{ textAlign: "left", fontSize: "16px",fontFamily: "Montserrat" }}
          >
            Schedule: {item.schedule}
          </Typography>
        </CardContent>
      </Box>
    </Card>
  );
}