import React, {useState} from 'react'
import { AppBar, Box, Drawer, IconButton, Toolbar, Typography } from '@mui/material'
import {Link} from 'react-router-dom'
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import PersonIcon from '@mui/icons-material/Person';
import ExitToAppIcon from '@mui/icons-material/ExitToApp';
import "../styles/HeaderStyles.css";
import MenuIcon from '@mui/icons-material/Menu';

const Header = () => {
  const [mobileOpen, setMobileOpen] = useState(false)
  //handle menu click
  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen)
  }
  //menu drawer
  const drawer = (
    <Box onClick={handleDrawerToggle} sx={{textAlign:'center'}}>
            <Typography 
            color= {"black"}
            variant="h6"
            component="div"
            sx={{flexGrow: 1, my:2}}
            >
              <img src={process.env.PUBLIC_URL + '/soup.png'} alt="Soup Image" className="logo" />
              </Typography>
                <ul className="mobile-navigation">
                  <li>
                    <Link to={'/checkout'}><ShoppingCartIcon/></Link>
                  </li>
                  <li>
                    <Link to={'/myclass'}>My Class</Link>
                  </li>
                  <li>
                    <Link to={'/invoice'}>Invoice</Link>
                  </li>
                  <li>
                    <Link to={'/profile'}><PersonIcon style={{ color: 'orange' }}/></Link>
                  </li>
                  <li>
                    <Link to={'/'}><ExitToAppIcon/></Link>
                  </li>
                </ul>
    </Box>
  );
  return (
    <>
      <Box>
        <AppBar component={"nav"} sx={{bgcolor: "white"}}>
          <Toolbar>
            <IconButton color="black" aria-label='open drawer' edge='start' sx={{mr: 2, display:{ sm :"none"}}} onClick={handleDrawerToggle}>
              <MenuIcon />
            </IconButton>
            <Typography 
            color= {"black"}
            variant="h6"
            component="div"
            sx={{flexGrow: 1}}
            >
              <img src={process.env.PUBLIC_URL + '/soup.png'} alt="Soup Image" className="logo" />
              </Typography>
              <Box sx={{ display: { xs: "none", sm: "block" } } }>
                <ul className="navigation-menu">
                  <li>
                    <Link to={'/checkout'}><ShoppingCartIcon/></Link>
                  </li>
                  <li>
                    <Link to={'/myclass'}>My Class</Link>
                  </li>
                  <li>
                    <Link to={'/invoice'}>Invoice</Link>
                  </li>
                  <li>
                    <Link to={''}>|</Link>
                  </li>
                  <li>
                    <Link to={'/profile'}><PersonIcon style={{ color: 'orange' }}/></Link>
                  </li>
                  <li>
                    <Link to={'/'}><ExitToAppIcon/></Link>
                  </li>
                </ul>
              </Box>
          </Toolbar>
        </AppBar>
        <Box component="nav">
          <Drawer variant="temporary" open={mobileOpen} onClose={handleDrawerToggle} sx={{display:{xs:'block', sm:'none'}, "& .MuiDrawer-paper": {boxSizing: "border-box", width: "240px",}, }}>
            {drawer}
          </Drawer>
        </Box>
        <Box sx={{ p: 0 }}>
        <Toolbar />
        </Box>
      </Box>  
    </>
  )
}

export default Header