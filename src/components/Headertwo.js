import React, { useState } from 'react';
import { AppBar, Box, Drawer, IconButton, Toolbar, Typography } from '@mui/material';
import { Link } from 'react-router-dom';
import MenuIcon from '@mui/icons-material/Menu';
import "../styles/HeaderStyles.css";

const Headertwo = () => {
  const [mobileOpen, setMobileOpen] = useState(false);

  // handle menu click
  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  // menu drawer
  const drawer = (
    <Box onClick={handleDrawerToggle} sx={{ textAlign: 'center' }}>
      <Typography
        color="black"
        variant="h6"
        component="div"
        sx={{ flexGrow: 1, my: 2 }}
      >
        <img
          src={process.env.PUBLIC_URL + '/soup.png'}
          alt="Soup Image"
          className="logo"
        />
      </Typography>
      <ul className="mobile-navigation">
        <li>
          <Link to="/login">Login</Link>
        </li>
        <li>
          <Link to="/register">Register</Link>
        </li>
      </ul>
    </Box>
  );

  return (
    <>
      <Box>
        <AppBar component="nav" sx={{ bgcolor: 'transparent' }}>
          <Toolbar>
            <IconButton
              color="black"
              aria-label="open drawer"
              edge="start"
              sx={{ mr: 2, display: { sm: 'none' } }}
              onClick={handleDrawerToggle}
            >
              <MenuIcon />
            </IconButton>
            <Typography
              color="black"
              variant="h6"
              component="div"
              sx={{ flexGrow: 1 }}
            >
              <img
                src={process.env.PUBLIC_URL + '/soup.png'}
                alt="Soup Image"
                className="logo"
              />
            </Typography>
            <Box
              component="div"
              sx={{
                display: { xs: 'none', sm: 'block' },
                background: 'transparent',
                padding: '10px',
                borderRadius: '5px',
              }}
            >
              <ul className="navigation-menu">
                <li>
                  <Link to="/login">Login</Link>
                </li>
                <li>
                  <Link to="/register">Register</Link>
                </li>
              </ul>
            </Box>
          </Toolbar>
        </AppBar>
        <Box component="nav">
        </Box>
        <Box sx={{ p: 0 }}>
          <Toolbar />
        </Box>
      </Box>
    </>
  );
};

export default Headertwo;