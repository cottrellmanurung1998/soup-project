import React from 'react';
import { AppBar, Toolbar, Typography,useTheme,useMediaQuery,Box, Button, IconButton } from '@mui/material';
import PaymentIcon from '@mui/icons-material/Payment';

function FooterCheckout({priceCount,onclick}) {
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("md"));

  return (
    <AppBar position="fixed" sx={{
      backgroundColor:'#ffffff',
      top: 'auto', 
      bottom: 0,
      width: '100%',
      // boxShadow: '0px -4px 4px -4px rgba(0, 0, 0, 0.2)',
     }}>
    <Toolbar
      sx={{
        marginX: isMobile ? 0 : '50px'
      }}
    >
      <Box sx={{ display:'flex', alignItems:'left', marginY:4 }}>
      <Typography variant='subtitle1'
              sx={{
                fontFamily: "Montserrat",
                fontSize: "18px",
                fontWeight: 500,
                color:'text.primary',
                marginRight: isMobile ? '5px' : '25px',
                marginTop:'5px'
              }}
            >
              Total Price:
            </Typography>

            <Typography variant='subtitle1'
              sx={{
                fontFamily: "Montserrat",
                fontSize: "24px",
                fontWeight: 600,
                color:'#226957'
              }}
            >
              IDR {priceCount}
            </Typography>
      </Box>
      <Box sx={{ flexGrow: 1 }} />

          {isMobile ? (
            <Box>
              <IconButton sx={{ color:'#FABC1D',display:'flex',justifyContent:'center',paddingX:'20px' }} onClick={onclick}>
                <PaymentIcon/>
              </IconButton>
            </Box>
          ) : (
            <Box
            sx={{ display: { xm: "flex", md: "flex" }, alignItems: "right" }}
          >
              <Button variant="contained" sx={{ backgroundColor:'#FABC1D',display:'flex',
                    justifyContent:'center',paddingX: '50px' }} onClick={onclick}>
                <Typography
                  sx={{
                    fontFamily: "Montserrat",
                    fontSize: "16px",
                    color: "#ffffff",
                    fontWeight: 500,
                    lineHeight:'auto'
                  }}
                >
                  Pay Now
                </Typography>
              </Button>
            </Box>
          )}
    </Toolbar>
  </AppBar>
  );
}

export default FooterCheckout;
