import { styled } from "@mui/material/styles";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import Button from "@mui/material/Button"; // Import Button component
import { matchPath, useLocation, useNavigate } from "react-router-dom";
import Typography from "@mui/material/Typography"; // Import Typography component
import Hooks from '../Hooks/hooks'
const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: "#5B4947",
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 16,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  "&:nth-of-type(even)": {
    backgroundColor: "#5B494733",
  },
  // hide last border
  "&:last-child td, &:last-child th": {
    border: 0,
  },
}));

function MyTable() {
  const { pathname } = useLocation();
  const matched = matchPath("/invoice/:id", pathname);
  const navigate = useNavigate(); // Initialize useNavigate

  const toDetails = (invoice_id) => {
    navigate(`/invoice/${invoice_id}`); // Navigates to the detail page of the selected invoice
  };

  const headerColumns = [
    {
      title: "No",
      props: {},
    },
    {
      title: "No Invoice",
      props: {
        align: "center",
      },
    },
    {
      title: "Date",
      props: {
        align: "center",
      },
    },
    {
      title: "Total Course",
      props: {
        align: "center",
      },
    },
    {
      title: "Total Price",
      props: {
        align: "center",
      },
    },
    {
      title: "Action",
      props: {
        align: "center",
      },
    },
  ];
  const apiurl = "http://localhost:5138/Invoices";
  const dataInvoice = Hooks(apiurl);

  const invoicedata = dataInvoice.map((item,index) => {
          
    return {
      no: index + 1,
      id_Invoice: item.id_Invoice,
      order_Date: item.order_Date,
      total_Course: item.total_Course,
      total_Price: item.total_Price,
      button_details: (
        <Button
          variant="outlined"
          size="large"
          sx={{ color: "gold", borderColor: "gold" }}
          onClick={() => toDetails("detailinvoice")}
        >
          Details
        </Button>
      )
  }
  })

  return (
    <TableContainer component={Paper}>
        {/* Tambahkan elemen Typography di sini untuk judul "Menu Invoice" */}
        <Typography variant="h5" gutterBottom sx={{ fontWeight: "bold", marginLeft: "12px" }}>
        Menu Invoice
      </Typography>
      <Table sx={{ minWidth: 700 }} aria-label="customized table">
        <TableHead>
          <TableRow>
            {headerColumns.map((col, index) => (
              <StyledTableCell key={index} sx={{ fontWeight: 600 }} {...col.props}>
                {col.title}
              </StyledTableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {invoicedata.map((row, index) => (
            <StyledTableRow key={index}>
              <StyledTableCell component="th" scope="row">
                {row.no}
              </StyledTableCell>
              <StyledTableCell align="center">
                {row.id_Invoice}
              </StyledTableCell>
              <StyledTableCell align="center">{row.order_Date}</StyledTableCell>
              <StyledTableCell align="center">
                {row.total_Course}
              </StyledTableCell>
              <StyledTableCell align="center">
                {row.total_Price}
              </StyledTableCell>
              <StyledTableCell align="center">
  {matched ? (
    row.button_details ? (
      <Button
        variant="outlined"
        size="large"
        sx={{ color: "gold", borderColor: "gold" }}
        onClick={() => toDetails(row.no_invoice)}
      >
        {row.button_details}
      </Button>
    ) : null
  ) : (
    <Button variant="outlined" size="large">
      {row.button_details}
    </Button>
  )}
</StyledTableCell>


            </StyledTableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}

MyTable.propTypes = {
  // ... sama seperti sebelumnya ...
};

export default MyTable;
