import React from "react";
import CallIcon from '@mui/icons-material/Call';
import InstagramIcon from "@mui/icons-material/Instagram";
import YouTubeIcon from "@mui/icons-material/YouTube";
import TelegramIcon from '@mui/icons-material/Telegram';
import MailOutlineIcon from '@mui/icons-material/MailOutline';
import { Box, Typography, Grid } from "@mui/material";

const Footer = () => {
  return (
    <>
      <Box sx={{ textAlign: "center", bgcolor: "#5B4947", color: "white", p: 12 }}>
        <Grid container spacing={9}>
          {/* Kolom 1: About Us */}
          <Grid item xs={12} sm={4} textAlign="left">
            <Typography variant="h6" sx={{ marginBottom: 2 }} color="goldenrod">
              About Us
            </Typography>
            {/* Konten Kolom 1 */}
            <Typography variant="body2" textAlign="justify">
            Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
            </Typography>
          </Grid>

          {/* Kolom 2: Product */}
          <Grid item xs={12} sm={4} textAlign="left">
            <Typography variant="h6" sx={{ marginBottom: 1 }} color="goldenrod">
              Product
            </Typography>
            {/* Konten Kolom 2 */}
            <Grid container spacing={1}>
              <Grid item xs={6}>
                <Typography variant="body2" sx={{ marginTop: 1 }} textAlign="justify">
                  <ul>
                    <li>Electric</li>
                    <li style={{ marginTop: "1rem" }}>LCGC</li>
                    <li style={{ marginTop: "1rem" }}>Offroad</li>
                    <li style={{ marginTop: "1rem" }}>SUV</li>
                  </ul>
                </Typography>
              </Grid>
              <Grid item xs={6}>
                <Typography variant="body2" sx={{ marginTop: 1 }} textAlign="justify">
                  <ul>
                    <li>Hatchback</li>
                    <li style={{ marginTop: "1rem" }}>MPV</li>
                    <li style={{ marginTop: "1rem" }}>Sedan</li>
                    <li style={{ marginTop: "1rem" }}>Truck</li>
                  </ul>
                </Typography>
              </Grid>
            </Grid>
          </Grid>

          {/* Kolom 3: Address */}
          <Grid item xs={12} sm={4} textAlign="left">
            <Typography variant="h6" sx={{ marginBottom: 2 }} color="goldenrod">
              Address
            </Typography>
            {/* Konten Kolom 3 */}
            <Typography variant="body2" textAlign="justify">
            Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque.
              <Typography variant="h6" sx={{ marginTop: 3 }} color="goldenrod">
              Contact Us
            </Typography>
              <Box
                sx={{
            my: 1,
            "& svg": {
              fontSize: "50px",
              cursor: "pointer",
              mr: 2,
            },
            "& svg:hover": {
              color: "goldenrod",
              transform: "translateX(5px)",
              transition: "all 400ms",
            },
          }}
        >
          {/* Icons */}
          <CallIcon />
          <InstagramIcon />
          <YouTubeIcon />
          <TelegramIcon />
          <MailOutlineIcon />
        </Box>
            </Typography>
          </Grid>
        </Grid>
      </Box>
    </>
  );
};

export default Footer;
