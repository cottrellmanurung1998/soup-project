import React, { useState } from 'react';
import Layouttwo from '../components/Layouttwo';

const Createpw = () => {
    // State to hold the form inputs
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
  
    // Function to handle form submission
    const handleSubmit = (event) => {
      event.preventDefault();
      // Check if the passwords match
      if (password === confirmPassword) {
        // Password creation logic goes here
        console.log('Password created successfully:', password);
        // You can call an API to handle password creation or any other logic as needed.
        // For now, just printing the password to the console for demonstration purposes.
      } else {
        // Passwords do not match, show an error message or take appropriate action.
        console.log('Passwords do not match.');
      }
    };

  return (
    <Layouttwo>
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'center',
          height: '20vh',
        }}
      > </div>
      <div className="create-password-container" style={{
        marginLeft: '420px',
      }}>


        <h2 className="create-password-heading">Create Password</h2>
        <form onSubmit={handleSubmit}>
          <div className="input-container">
            <input
              type="password"
              id="password"
              placeholder="New Password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              required
            />
          </div>
          <div className="input-container">
            <input
              type="password"
              id="confirm"
              placeholder="Confirm New Password"
              value={confirmPassword}
              onChange={(e) => setConfirmPassword(e.target.value)}
              required
            />
          </div>
          <div className="button-container">
            <button type="button" className="cancel-button" onClick={() => console.log('Cancel')}>
              Cancel
            </button>
            <button type="submit" className="confirm-button">
              Confirm
            </button>
          </div>
        </form>
      </div>
    </Layouttwo>
  );
};

export default Createpw;
