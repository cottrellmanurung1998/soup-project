import React from 'react';
import Layouttwo from '../components/Layouttwo';
import { Link } from 'react-router-dom';
import '../styles/Register.css';

const Register = () => {
  const handleLogin = (event) => {
    event.preventDefault();
    const email = document.getElementById('email').value;
    const password = document.getElementById('password').value;
    // Perform login logic here
  };

  return (
    <Layouttwo>
      <div className="container">
        <form>
        <h2> Are you ready become a professional cheff?</h2>
        <p className="required">Please Register First</p>
          <div className="input-container">
            <input type="text" id="text" placeholder="Nama" required />
          </div>
          <div className="input-container">
            <input type="email" id="email" placeholder="Email" required />
          </div>
          <div className="input-container">
            <input type="password" id="password" placeholder="Password" required />
          </div>
          <div className="input-container">
            <input type="password" id="password" placeholder="Confirm Password" required />
          </div>
          <div className="forgot-link">
            <p>
              Forgot Password? <Link to="/resetpw">Click here</Link>
            </p>
          </div>
          <div className="button-container">
          <Link to="/login">
            <button type="submit">
          Sign up
          </button> </Link>

          </div>
        </form>
        <p className="signup-link">
          Have account? <Link to="/login">Login here</Link>
        </p>
      </div>
    </Layouttwo>
  );
};

export default Register;
