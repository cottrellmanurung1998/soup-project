import axios from 'axios';

const instance = axios.create({
  baseURL: 'http://localhost:5138/MyClass', // Ganti dengan URL backend Anda
});

export default instance;
