import React, { useState } from 'react';
import { Box, Button, Select, MenuItem } from '@mui/material';
import Layout from '../components/Layout';
import tomyumImage from '../images/tomyum.png';
import sushiImage from '../images/sushi.png';
import ramenImage from '../images/ramen.png';
import rendangImage from '../images/rendang.png';
import pempekImage from '../images/pempek.png';
import sotoImage from '../images/soto.png';
import takoyakiImage from '../images/takoyaki.png';
import jajangmyeonImage from '../images/jajangmyeon.png';
import sateImage from '../images/sate.png';
import { Height } from '@mui/icons-material';

const Tomyum = () => {
  const [selectedSchedule, setSelectedSchedule] = useState(''); 

  const handleScheduleChange = (event) => {
    setSelectedSchedule(event.target.value);
  };

  return (
    <Layout>
      <Box sx={{ display: 'flex', alignItems: 'center', gap: '20px', marginRight: '25px', marginLeft: '25px', marginTop: '25px' }}>
        <div className="image">
         <img src={tomyumImage} alt="Tomyum Image" style={{ width: '400px', Height: "300px" }} />
        </div>
        <div className="details">
          <Box className="title-price">
            <h3 style={{ fontWeight: 'normal', fontSize: '20px' }}>Asian</h3>
            <h2>Tom Yum Thailand</h2>
            <p style={{ fontSize: '24px' }}>IDR 450.000</p>
          </Box>
          <Box className="menu">
            <Box className="schedule" style={{ marginBottom: '10px' }} > 
              <Select
                id="schedule"
                value={selectedSchedule}
                onChange={handleScheduleChange}
              >
                 <MenuItem disabled value="">
                  Select Schedule
                </MenuItem>
                <MenuItem value="Monday, 25 July 2022">Monday, 25 July 2022</MenuItem>
                <MenuItem value="Tuesday, 26 July 2022">Tuesday, 26 July 2022</MenuItem>
                <MenuItem value="Wednesday, 27 July 2022">Wednesday, 27 July 2022</MenuItem>
                <MenuItem value="Thursday, 28 July 2022">Thursday, 28 July 2022</MenuItem>
                <MenuItem value="Friday, 29 July 2022">Friday, 29 July 2022</MenuItem>
                <MenuItem value="Saturday, 30 July 2022">Saturday, 30 July 2022</MenuItem>
              </Select>
            </Box>
            <Box className="buttons">
              <Button style={{ marginRight: '10px' }}>Add to Cart</Button>
              <Button>Buy Now</Button>
            </Box>
          </Box>
        </div>
      </Box>
      <Box className="description" sx={{marginRight: '25px', marginLeft: '25px' }}>
        <h3>Description</h3>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
          Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
          Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
          Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
        </p>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
          Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
          Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
          Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
        </p>
      </Box>
      <hr className="divider" />
      <section className="professional-class">
        <h3>Another menu in this class</h3>
        <Box className="image-row">
          <Box className="image-box">
            <img src={sushiImage} alt="Sushi Image" />
            <h1>Asian</h1>
            <h2>Sushi Salmon with Mentai</h2>
            <h3>IDR 200.000</h3>
          </Box>
          <Box className="image-box">
            <img src={ramenImage} alt="Ramen Image" />
            <h1>Asian</h1>
            <h2>Ichiraku Ramen</h2>
            <h3>IDR 300.000</h3>
          </Box>
          <Box className="image-box">
            <img src={rendangImage} alt="Rendang Image" />
            <h1>Asian</h1>
            <h2>Rendang Sapi</h2>
            <h3>IDR 200.000</h3>
          </Box>
        </Box>
        <Box className="image-row">
          <Box className="image-box">
            <img src={pempekImage} alt="Pempek Image" />
            <h1>Asian</h1>
            <h2>[Complit Package]<br />Pempek Palembang</h2>
            <div className="price-wrapper">
              <h3>IDR 600.000</h3>
            </div>
          </Box>
          <Box className="image-box">
            <img src={sotoImage} alt="Soto Image" />
            <h1>Asian</h1>
            <h2>Soto Banjar Limau Kuit</h2>
            <h3>IDR 150.000</h3>
          </Box>
          <Box className="image-box">
            <img src={takoyakiImage} alt="Takoyaki Image" />
            <h1>Asian</h1>
            <h2>Takoyaki Octopus</h2>
            <h3>IDR 150.000</h3>
          </Box>
        </Box>
        <Box className="image-row">
          <Box className="image-box">
            <img src={jajangmyeonImage} alt="Jajangmyeon Image" />
            <h1>Asian</h1>
            <h2>Jajangmyeon</h2>
            <h3>IDR 250.000</h3>
          </Box>
          <Box className="image-box">
            <img src={sateImage} alt="Sate Image" />
            <h1>Asian</h1>
            <h2>Sate Padang</h2>
            <h3>IDR 300.000</h3>
          </Box>
        </Box>
      </section>
    </Layout>
  );
};

export default Tomyum;