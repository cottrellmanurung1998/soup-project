import React from 'react';
import Layouttwo from '../components/Layouttwo';
import bannerImage from '../images/banatas.png';
import cookiesImage from '../images/cookies.png';
import greencakeImage from '../images/greencake.png';
import iceImage from '../images/ice.png';
import sotoImage from '../images/soto.png';
import spagettiImage from '../images/spagetti.png';
import tomyumImage from '../images/tomyum.png';
import middleImage from '../images/middle.png';
import asianImage from '../images/asian.png';
import coldImage from '../images/cold.png';
import cookies2Image from '../images/cookies2.png';
import desertImage from '../images/desert.png';
import easternImage from '../images/eastern.png';
import hotImage from '../images/hot.png';
import junkfoodImage from '../images/junkfood.png';
import westernImage from '../images/western.png';
import '../styles/home.css';
import { Link } from 'react-router-dom';
import { BrowserRouter, Route } from 'react-router-dom';
import CourseAsian from '../pages/courseasian.js';

const App = () => {
  return (
    <BrowserRouter>
      <Route exact path="/" component={Landingpage} />
      <Route path="/courseasian" component={CourseAsian} />
    </BrowserRouter>
  );
};

const Landingpage = () => {
  return (
    <Layouttwo>
      <section className="banner">
        <img src={bannerImage} alt="Banner Image" />
        <h1>Be the next great chef</h1>
        <h2>
          We are able to awaken your cooking skills to become a classy chef and ready to dive into the professional world
        </h2>
      </section>
      <section className="boxes">
        <div className="box">
          <p>200+</p>
          <p1>Various cuisines available in professional class</p1>
        </div>
        <div className="box">
          <p>50+</p>
          <p1>A chef who is reliable and has his own characteristics in cooking</p1>
        </div>
        <div className="box">
          <p>30+</p>
          <p1>Cooperate with trusted and upscale restaurants</p1>
        </div>
      </section>
      <section className="professional-class">
        <h3>More Professional Class</h3>
        <div className="image-row">
          <div className="image-box">
            <img src={tomyumImage} alt="Tomyum Image" />
            <h1>Asian</h1>
            <h2>Tom Yum Thailand</h2>
            <h3>IDR 46.000</h3>
          </div>
          <div className="image-box">
            <img src={iceImage} alt="Ice" />
            <h1>Cold Drink</h1>
            <h2>Strawberry Float</h2>
            <h3>IDR 150.000</h3>
          </div>
          <div className="image-box">
            <img src={cookiesImage} alt="Cookies" />
            <h1>Cookies</h1>
            <h2>Chocolate Cookies</h2>
            <h3>IDR 200.000</h3>
          </div>
        </div>
        <div className="image-row">
          <div className="image-box">
            <img src={greencakeImage} alt="Greentea" />
            <h1>Dessert</h1>
            <h2>Green Tea Cheesecake</h2>
            <h3>IDR 400.000</h3>
          </div>
          <div className="image-box">
            <img src={sotoImage} alt="Soto" />
            <h1>Asian</h1>
            <h2>Soto Banjar Limau Kuit</h2>
            <h3>IDR 150.000</h3>
          </div>
          <div className="image-box">
            <img src={spagettiImage} alt="Spagetti" />
            <h1>Western</h1>
            <h2>Italian Spaghetti Bolognese</h2>
            <h3>IDR 450.000</h3>
          </div>
        </div>
      </section>
      <section className="middle">
        <img src={middleImage} alt="Middle Image" />
        <h1>Gets your best benefit</h1>
        <h2>
          Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem
          aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
          Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores
          eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet,
          consectetur, adipisci velit, sed quia non numquam.
        </h2>
      </section>
      <section className="food-type">
        <h3>More food type as you can choose</h3>
        <div className="image-row2">
          <div className="image-box2">
          <Link to="/courseasian">
          <img src={asianImage} alt="Asian" />
          </Link>
            <h1>Asian</h1>
          </div>
          <div className="image-box2">
            <img src={coldImage} alt="Cold Drink" />
            <h1>Cold Drink</h1>
          </div>
          <div className="image-box2">
            <img src={cookies2Image} alt="Cookies" />
            <h1>Cookies</h1>
          </div>
          <div className="image-box2">
            <img src={desertImage} alt="Desert" />
            <h1>Desert</h1>
          </div>
        </div>
        <div className="image-row2">
          <div className="image-box2">
            <img src={easternImage} alt="Eastern" />
            <h1>Eastern</h1>
          </div>
          <div className="image-box2">
            <img src={hotImage} alt="Hot Drink" />
            <h1>Hot Drink</h1>
          </div>
          <div className="image-box2">
            <img src={junkfoodImage} alt="Junkfood" />
            <h1>Junkfood</h1>
          </div>
          <div className="image-box2">
            <img src={westernImage} alt="Western" />
            <h1>Western</h1>
          </div>
        </div>
      </section>
    </Layouttwo>
  );
};

export default Landingpage;