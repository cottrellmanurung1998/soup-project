import React from 'react';
import Layouttwo from '../components/Layouttwo';
import '../styles/reset.css';

const Reset = () => {
  const handleLogin = (event) => {
    event.preventDefault();
    const email = document.getElementById('email').value;
  };

  return (
    <Layouttwo>
      <div className="container">
        <form>
        <h2> Reset Password </h2>
        <p className="required">Send OTP Code to your email address</p>
          <div className="input-container">
            <input type="email" id="email" placeholder="Email" required />
          </div>
          <div className="button-container">
            <button type="submit" onClick={handleLogin}> Confirm </button>
            <button type="submit" onClick={handleLogin}> Cancel </button>
          </div>
        </form>
      </div>
    </Layouttwo>
  );
};

export default Reset;