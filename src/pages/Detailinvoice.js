import React from 'react'
import Layout from '../components/Layout'
import Detailinvoicetable from '../components/Detailinvoicetable';

const Detailinvoice = () => {
  return (
    <Layout>
        <h1>
            <Detailinvoicetable />
        </h1>
    </Layout>
  )
}

export default Detailinvoice