import { Box, Button, Typography } from "@mui/material";
import SuccessIllustration from "../images/success_illustration.svg";
import Logo from "../images/soup.png"; // Import the logo image
import { useNavigate } from "react-router";

export default function SuccessPayment() {
  const navigate = useNavigate();
  const toNavigate = (page) => {
    if (page === "invoice") {
      return navigate("/invoice");
    }

    if (page === "home") {
      return navigate("/");
    }
  };

  return (
    <>
      <Box
        width="fullWidth"
        sx={{
          position: "relative",
        }}
      >
        <Box
          sx={{
            position: "absolute",
            top: "10px", // Adjust the top position to position the logo
            left: "10px", // Adjust the left position to position the logo
          }}
        >
          <img src={Logo} alt="logo" width="70" height="70" />
        </Box>
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, 50%)",
            textAlign: "center",
            width: "90%",
          }}
        >
          <Box>
            <img src={SuccessIllustration} alt="icon-success" />
          </Box>
          <Box>
            <Typography
              variant="h5"
              color="#5B4947"
              sx={{ fontWeight: "500", marginBottom: "8px" }}
            >
              Purchased Successfully
            </Typography>
            <Typography
              component="p"
              color="#5B4947"
              sx={{ marginBottom: "40px" }}
            >
              That's Great! We're ready for the driving day
            </Typography>

            <Button
            color="primary"
            variant="contained"
              sx={{
                padding: "16px 24px",
                marginRight: "10px",
                color: "#5B4947", backgroundColor: "white", "&:hover": { backgroundColor: "white"
              }} }
              onClick={() => toNavigate("home")}
            >
              <Typography component="span" sx={{ fontWeight: 600 }}>
                Back to Home
              </Typography>
            </Button>
            <Button
            color="primary" sx={{ padding: "16px 30px", marginRight: "10px", color: "white", backgroundColor: "gold", "&:hover": { backgroundColor: "gold" } }}
            onClick={() => toNavigate("invoice")}>
              <Typography component="span" sx={{ fontWeight: 600 }} color="#5B4947">
                Open Invoice
              </Typography>
            </Button>
          </Box>
        </Box>
      </Box>
    </>
  );
}
