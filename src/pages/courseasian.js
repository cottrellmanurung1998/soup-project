import React from 'react';
import Layout from '../components/Layout';
import asianbannerImage from '../images/asianbanner.png';
import sotoImage from '../images/soto.png';
import pempekImage from '../images/pempek.png';
import sushiImage from '../images/sushi.png';
import ramenImage from '../images/ramen.png';
import rendangImage from '../images/rendang.png';
import takoyakiImage from '../images/takoyaki.png';
import jajangmyeonImage from '../images/jajangmyeon.png';
import sateImage from '../images/sate.png';
import tomyumImage from '../images/tomyum.png';
import Tomyum from '../pages/tomyum.js';
import Landingpage from '../pages/landingpage.js';
import { Link } from 'react-router-dom';
import { BrowserRouter, Route } from 'react-router-dom';

const App = () => {
  return (
    <BrowserRouter>
      <Route exact path="/" component={Landingpage} />
      <Route path="/tomyum" component={Tomyum} />
    </BrowserRouter>
  );
};

const courseasian = () => {
  return (
    <Layout>
       <div>
      <section className="banner">
        <img src={asianbannerImage} alt="Asian Banner Image" />
      </section>
      <section className="boxes">
        <p className="lorem-ipsum">
          <span style={{ fontSize: '24px' }}>
            <strong>Asian</strong>
          </span>
          <br />
          <br />
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
          laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat
          non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
        </p>
      </section>
      <hr className="divider" />
      <section className="professional-class">
        <h3>Another menu in this class</h3>
        <div className="image-row">
          <div className="image-box">
            <img src={sushiImage} alt="Sushi Image" />
            <h1>Asian</h1>
            <h2>Sushi Salmon with Mentai</h2>
            <h3>IDR 200.000</h3>
          </div>
          <div className="image-box">
            <img src={ramenImage} alt="Ramen Image" />
            <h1>Asian</h1>
            <h2>Ichiraku Ramen</h2>
            <h3>IDR 300.000</h3>
          </div>
          <div className="image-box">
            <img src={rendangImage} alt="Rendang Image" />
            <h1>Asian</h1>
            <h2>Rendang Sapi</h2>
            <h3>IDR 200.000</h3>
          </div>
        </div>
        <div className="image-row">
          <div className="image-box">
            <img src={pempekImage} alt="Pempek Image" /> 
            <h1>Asian</h1>
            <h2>
              [Complit Package]
              <br />
              Pempek Palembang
            </h2>
            <div className="price-wrapper">
              <h3>IDR 600.000</h3>
            </div>
          </div>
          <div className="image-box">
            <img src={sotoImage} alt="Soto Image" />
            <h1>Asian</h1>
            <h2>Soto Banjar Limau Kuit</h2>
            <h3>IDR 150.000</h3>
          </div>
          <div className="image-box">
             <Link to="/tomyum">
             <img src={tomyumImage} alt="Tomyum" />
             </Link>
            <h1>Asian</h1>
            <h2>Tom Yum Thailand</h2>
            <h3>IDR 450.000</h3>
          </div>
        </div>
        <div className="image-row">
          <div className="image-box">
            <img src={takoyakiImage} alt="Takoyaki Image" /> 
            <h1>Asian</h1>
            <h2>Takoyaki Octopus</h2>
            <h3>IDR 150.000</h3>
          </div>
          <div className="image-box">
            <img src={jajangmyeonImage} alt="Jajangmyeon Image" /> 
            <h1>Asian</h1>
            <h2>Jajangmyeon</h2>
            <h3>IDR 250.000</h3>
          </div>
          <div className="image-box">
            <img src={sateImage} alt="Sate Image" /> 
            <h1>Asian</h1>
            <h2>Sate Padang</h2>
            <h3>IDR 300.000</h3>
          </div>
        </div>
      </section>
    </div>
    </Layout>
  );
};

export default courseasian;