import React, { useState } from 'react';
import Layouttwo from '../components/Layouttwo';
import { Link } from 'react-router-dom';

const Login = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [errorMessage, setErrorMessage] = useState('');

  const handleLogin = (event) => {
    event.preventDefault();
    // Perform basic login validation (for demonstration only)
    if (email === 'user@example.com' && password === 'password123') {
      // Redirect to the home page after successful login
      window.location.href = '/home';
    } else {
      setErrorMessage('Invalid email or password');
    }
  };

  return (
    <Layouttwo>
      <div className="container">
        <form onSubmit={handleLogin}>
          <h2>Welcome Back! Cheff</h2>
          <p className="required">Please Login First</p>
          <div className="input-container">
            <input
              type="email"
              id="email"
              placeholder="Email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              required
            />
          </div>
          <div className="input-container">
            <input
              type="password"
              id="password"
              placeholder="Password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              required
            />
          </div>
          <div className="forgot-link">
            <p>
              Forgot Password? <Link to="/resetpw">Click here</Link>
            </p>
          </div>
          <div className="button-container">
            <button type="submit">Login</button>
          </div>
        </form>
        <p className="signup-link">
          Don't have an account? <Link to="/register">Sign up here</Link>
        </p>
        {errorMessage && <p className="error-message">{errorMessage}</p>}
      </div>
    </Layouttwo>
  );
};

export default Login;
