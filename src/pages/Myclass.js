import React from 'react'
import Layout from '../components/Layout'
import {
  Box,
  Grid,
  CardActionArea,
  CardContent,
  CardMedia,
  Typography,
} from "@mui/material";
import Hooks from '../Hooks/hooks'
import ClassList from '../components/myclasscart'
const Myclass = () => {
  const url = "http://localhost:5138/MyClass"
  const dataClass = Hooks (url);

  return (
    <Layout>
      <Grid
        container
        spacing={0}
        direction="column"
        alignItems="center"
        sx={{
          border: 1,
          borderColor: "black",
          width: "100%",
          minHeight: "800px",
        }}
      >
        {dataClass.length > 0 ? (
          dataClass.map((menu) => {
            console.log(menu)
            return (
              <ClassList item={menu}/>
            )
          })
        ) : (
          <div>No data available</div>
        )}
      </Grid>
    </Layout>
  );
}  

export default Myclass;