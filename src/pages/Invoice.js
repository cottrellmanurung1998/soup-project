import React from 'react'
import Layout from '../components/Layout'
import Invoicetable from '../components/Invoicetable'

const Invoice = () => {
  return (
    <Layout>
      <Invoicetable />
      </Layout>
  )
}

export default Invoice